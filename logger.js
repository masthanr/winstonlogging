const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');
const fs = require('fs');
const nodemailer = require('nodemailer');
const moment = require('moment');
require('winston-mongodb');
require('winston-mail');
const isDB = false;

const logDir = 'loggerPath';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const logConfig = {
    filename : `${logDir}/%DATE%-loggerFile.log`
}
const database = {
    db  : 'mongodb://localhost:27017/Logger',
    level: 'info',
};
const mailOptions = {
    to: 'masthan.reddy@novatium.com',
    from: 'masthanr1354@gmail.com',
    subject: 'An Error Occured On Server. Please Check IT ASAP',
    username: 'masthanr1354@gmail.com',
    password: 'pass',
    level: 'error',
    host:"smtp.gmail.com",
    msg:'Error',
    ssl:true
};
const emailConfig = {
    fromEmail:'masthanr1354@gmail.com',
    password:'password',
    toEmail:'masthan.reddy@novatium.com'
}

const MESSAGE = Symbol.for('message');
const jsonFormatter = (logEntry) => {
    const base = { timestamp: moment().format('MMMM Do YYYY, h:mm:ss A; ') };
    const json = Object.assign(base, logEntry);
    logEntry[MESSAGE] = JSON.stringify(json);
    return logEntry;
};
let logging = {
    logFileDirName: 'logs',
    console: {
        level: 'degug',
        colorize: true,
        handleExceptions: true,
        json: false
    },
    file: {
        timestamp: true,
        json: false,
        datePattern: 'YYYY-MM-DD',
        filename: 'LoggerFile',
        maxsize: 10000000, // 10MB
        level: 'info',
        zippedArchive: false
    }
};
const sendEmail= async (level,message,data) => {
    try {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: emailConfig.fromEmail,
                pass: emailConfig.password
            }
        });
        const mailOptions = {
            from: `Logger <${emailConfig.fromEmail}>`,
            to: emailConfig.toEmail,
            replyTo: `noreply.${emailConfig.fromEmail}`,
            subject: 'An Error Occured On Server. Please Check IT ASAP',
            text: message
        };

        const test = await transporter.sendMail(mailOptions);
        console.log('Mail sent successfully',test.messageId)
        if (test.messageId) {
            return true;
        }
    }catch(e){
        console.log('Error While sending Message to user',e)
    }
};
class Logger {
    constructor() {
        this.winston = winston.createLogger({
            format: winston.format(jsonFormatter)(),
            transports: [
                //new winston.transports.Console(logging),
                new DailyRotateFile(logConfig),
                new winston.transports.MongoDB(database),
                //new winston.transports.Mail(mailOptions)
            ],
            exitOnError: false
        });
    }

    async log(level, message, data) {
        if (data) {
            this.winston.log(level, message, data);
            await sendEmail(level,message,data);
        } else {
            this.winston.log(level, message);
            await sendEmail(level,message,data);
        }
    }
    error(message, data) {
        this.log('error', message, data);
    }
    warn(message, data) {
        this.log('warn', message, data);
    }
    info(message, data) {
        this.log('info', message, data);
    }
    debug(message, data) {
        this.log('debug', message, data);
    }
}
module.exports = new Logger();

